export const main = () => {
    document
        .querySelector(".alertCookieButton")
        .addEventListener("click", alertCookie);
    document
        .querySelector(".setCookieButton")
        .addEventListener("click", setCookie);
    document
        .querySelector(".setFakeCookieButton")
        .addEventListener("click", setFakeCookie);
};

const alertCookie = () => {
    alert(document.cookie);
};

const setCookie = () => {
    document.cookie =
        "connect.sid=s%3A4ViJA7itun7Re4l1h9IqsNELghOZ9pwy.hmYbLvE4rP%2BuizlZ8sOCPxjFvIe0rHHk7X5MwhwdGTw;domain=.qoom.space;path=/";
    alert("cookie set");
};

const setFakeCookie = () => {
    document.cookie = "connect.sid=NotAFakeCookie;domain=.qoom.space;path=/";
    alert("Invalid cookie set");
};

main().then();
